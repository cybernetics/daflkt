// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ComposedExpressionTest {
    @Test
    fun `de morgan with and falsy`() {
        assertFalse(DaflQuery("not (test:1234 and test2:value)").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `de morgan with and truthy`() {
        assertTrue(DaflQuery("not (test:1234 and test2:value)").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "value")
        )))
    }

    @Test
    fun `de morgan with or falsy`() {
        assertFalse(DaflQuery("not (test:1234 or test2:value)").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `de morgan with or truthy`() {
        assertTrue(DaflQuery("not (test:1234 and test2:value)").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "val")
        )))
    }
}
