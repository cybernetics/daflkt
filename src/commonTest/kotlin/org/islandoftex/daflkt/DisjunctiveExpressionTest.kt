// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class DisjunctiveExpressionTest {
    @Test
    fun `disjunctive expression symbol`() {
        assertTrue(DaflQuery("test:1234 | test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "val")
        )))
    }

    @Test
    fun `disjunctive expression truthy`() {
        assertTrue(DaflQuery("test:1234 or test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `disjunctive expression truthy and falsy`() {
        assertTrue(DaflQuery("test:1234 or test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "val")
        )))
    }
    @Test
    fun `disjunctive expression truthy and missing`() {
        assertTrue(DaflQuery("test:1234 or test2:value").evaluate(listOf(
            Property("test", "1234")
        )))
    }
    @Test
    fun `disjunctive expression falsy and truthy`() {
        assertTrue(DaflQuery("test:1234 or test2:value").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `disjunctive expression falsy`() {
        assertFalse(DaflQuery("test:1234 or test2:value").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "val")
        )))
    }
}
