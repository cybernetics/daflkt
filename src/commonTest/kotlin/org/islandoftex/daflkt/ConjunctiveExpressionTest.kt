// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ConjunctiveExpressionTest {
    @Test
    fun `conjunctive expression keyword`() {
        assertTrue(DaflQuery("test:1234 and test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `conjunctive expression keyword falsy`() {
        assertFalse(DaflQuery("test:1234 and test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "val")
        )))
    }

    @Test
    fun `conjunctive expression truthy`() {
        assertTrue(DaflQuery("test:1234 test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `conjunctive expression truthy and falsy`() {
        assertFalse(DaflQuery("test:1234 test2:value").evaluate(listOf(
            Property("test", "1234"),
            Property("test2", "val")
        )))
    }
    @Test
    fun `conjunctive expression truthy and missing`() {
        assertFalse(DaflQuery("test:1234 test2:value").evaluate(listOf(
            Property("test", "1234")
        )))
    }
    @Test
    fun `conjunctive expression falsy and truthy`() {
        assertFalse(DaflQuery("test:1234 test2:value").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "value")
        )))
    }
    @Test
    fun `conjunctive expression falsy`() {
        assertFalse(DaflQuery("test:1234 test2:value").evaluate(listOf(
            Property("test", "123"),
            Property("test2", "val")
        )))
    }
}
