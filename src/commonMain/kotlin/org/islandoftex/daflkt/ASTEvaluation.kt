// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

/**
 * Evaluate a tree given as [DaflNode] by applying the matchers of each
 * property of [properties] to the [Leaf] nodes of the tree.
 *
 * Performs short-circuit evaluation wherever possible, i.e. for [ANDNode]s
 * and [ORNode]s.
 *
 * @return Boolean value representing a match or mismatch.
 */
internal fun DaflNode.evaluate(properties: List<Property>): Boolean =
    when (this) {
        is ANDNode -> {
            left.evaluate(properties) && right.evaluate(properties)
        }
        is ORNode -> {
            left.evaluate(properties) || right.evaluate(properties)
        }
        is NOTNode -> {
            !left.evaluate(properties)
        }
        is Leaf -> {
            properties.firstOrNull { it.fieldName == fieldName }
                ?.let { it.matcher(it.value, value) }
                ?: false
        }
    }
