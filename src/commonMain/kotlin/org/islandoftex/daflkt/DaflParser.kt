// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.daflkt

/**
 * Parse a [query] into a tree of [DaflNode]s.
 */
internal class DaflParser(private val query: String) {
    private fun sanitizeExpression(s: String): String =
        s.trim().removeSurrounding("(", ")")

    private fun nextWord(i: Int): String =
        query.substring(i).trim().substringBefore(" ")

    /**
     * First pass over [query] by splitting composed expressions. This will
     * apply [transformer] on every subexpression it encounters.
     */
    private fun performStringPass(transformer: (String) -> DaflNode): DaflNode? {
        if (query.startsWith("not")) {
            return NOTNode(transformer(sanitizeExpression(query.removePrefix("not"))))
        }

        var inString = false
        var inSubExpression = 0
        query.forEachIndexed { i, c ->
            when {
                c == '"' -> inString = !inString
                c == '(' && !inString -> inSubExpression += 1
                c == ')' && !inString -> inSubExpression -= 1
                c == '|' && !inString && inSubExpression == 0 -> {
                    return ORNode(
                        transformer(query.substring(0, i)),
                        transformer(query.substring(i+1))
                    )
                }
                c == ' ' && !inString && inSubExpression == 0 -> {
                    val nextWord = nextWord(i)
                    val leftExpr = sanitizeExpression(query.substring(0, i))
                    val rightExpr = sanitizeExpression(query.substring(i+1).substringAfter(nextWord))
                    return when (nextWord) {
                        "|", "or" -> ORNode(
                            transformer(leftExpr),
                            transformer(rightExpr)
                        )
                        "and" -> ANDNode(
                            transformer(leftExpr),
                            transformer(rightExpr)
                        )
                        else -> {
                            // this was the and short symbol
                            ANDNode(
                                transformer(leftExpr),
                                transformer(query.substring(i + 1))
                            )
                        }
                    }
                }
            }
        }

        if (inString || inSubExpression != 0)
            throw DaflParseException("Ill-formed query because at the end there is either an open " +
                    "string or an open subexpression.")

        return null
    }

    /**
     * Parse the [valueQuery] part of `fieldName:valueQuery` for a given
     * [fieldName]. Value queries may contain Boolean operators, hence they
     * are treated like ordinary expressions with [fieldName] being passed
     * down to the leaves.
     */
    private fun parseValueQuery(fieldName: String, valueQuery: String): DaflNode {
        return performStringPass {
            parseValueQuery(fieldName, sanitizeExpression(it))
        } ?: when {
            valueQuery.startsWith("-\"") && valueQuery.endsWith('"') ->
                NOTNode(Leaf(fieldName, valueQuery.removePrefix("-").removeSurrounding("\"")))
            valueQuery.startsWith('"') && valueQuery.endsWith('"') ->
                Leaf(fieldName, valueQuery.removeSurrounding("\""))
            valueQuery.startsWith("-") ->
                NOTNode(Leaf(fieldName, valueQuery.removePrefix("-")))
            else ->
                Leaf(fieldName, valueQuery)
        }
    }

    /**
     * Convert the given [query] into its tree representation. This will not
     * keep structure intact, it will perform a few changes to simplify
     * Boolean interpretation.
     */
    fun toExpressionTree(): DaflNode {
        // composed queries
        val preProcessed = performStringPass {
            DaflParser(sanitizeExpression(it)).toExpressionTree()
        }
        if (preProcessed != null)
            return preProcessed

        // an atomic query has to be a field matcher and start with a key
        if (!query.contains(":"))
            throw DaflParseException("Atomic query '$query' should separate a field from its value.")

        val key = query.substringBefore(":")
        val sanitizedKey = key.removePrefix("-").trim()
        val valueQuery = query.substringAfter(":").trim()
        return when {
            valueQuery.startsWith('-') -> {
                NOTNode(parseValueQuery(sanitizedKey, valueQuery.removePrefix("-")))
            }
            valueQuery.startsWith('"') -> {
                if (!valueQuery.endsWith('"')) {
                    throw DaflParseException("Quoted strings must be balanced.")
                }
                Leaf(sanitizedKey, valueQuery.removeSurrounding("\""))
            }
            valueQuery.startsWith("(") -> {
                if (!valueQuery.endsWith(")")) {
                    throw DaflParseException("Field expressions have to be balanced.")
                }
                parseValueQuery(sanitizedKey, valueQuery.removeSurrounding("(", ")"))
            }
            else -> Leaf(sanitizedKey, valueQuery)
        }.let {
            if (key.startsWith("-")) {
                NOTNode(it)
            } else it
        }
    }
}
