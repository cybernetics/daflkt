import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    kotlin("multiplatform") version "2.0.0"

    id("org.jetbrains.dokka") version "1.9.20"
    id("com.diffplug.spotless") version "6.25.0"
    id("com.diffplug.spotless-changelog") version "3.0.2"
    id("com.github.ben-manes.versions") version "0.51.0"
    id("io.gitlab.arturbosch.detekt") version "1.23.6"

    id("maven-publish")
}

group = "org.islandoftex"
description = "A data filtering language in Kotlin"
version = project.spotlessChangelog.versionNext

repositories {
    mavenCentral()
}

kotlin {
    explicitApiWarning()
    jvm()

    with(sourceSets) {
        commonMain.dependencies {
            implementation(kotlin("stdlib-common"))
        }

        commonTest.dependencies {
            implementation(kotlin("test-common"))
            compileOnly(kotlin("test-annotations-common"))
        }

        jvmTest.dependencies {
            implementation(kotlin("test-junit5"))
            runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.10.2")
        }
    }

    compilerOptions {
        jvmToolchain(8)
    }
}

tasks {
    named<Test>("jvmTest") {
        useJUnitPlatform()

        testLogging {
            events(
                TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED,
                TestLogEvent.STANDARD_ERROR, TestLogEvent.STANDARD_OUT
            )
        }
    }

    dokkaHtml {
        dokkaSourceSets {
            configureEach {
                jdkVersion.set(8)
                moduleName.set("${project.group}.daflkt")
                includeNonPublic.set(false)
                skipDeprecated.set(false)
                reportUndocumented.set(true)
                skipEmptyPackages.set(true)
                platform.set(org.jetbrains.dokka.Platform.common)
                sourceLink {
                    localDirectory.set(file("./"))
                    remoteUrl.set(uri("https://gitlab.com/islandoftex/libraries/daflkt").toURL())
                    remoteLineSuffix.set("#L")
                }
                noStdlibLink.set(false)
                noJdkLink.set(false)
            }
        }
    }

    withType<DependencyUpdatesTask> {
        rejectVersionIf {
            val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { candidate.version.uppercase() in it }
            val isStable = stableKeyword || "^[0-9,.v-]+$".toRegex().matches(candidate.version)
            isStable.not()
        }
    }
}

spotless {
    kotlinGradle {
        target("build.gradle.kts", "settings.gradle.kts")
        trimTrailingWhitespace()
        endWithNewline()
    }

    kotlin {
        licenseHeader("// SPDX-License-Identifier: BSD-3-Clause")
        target("src/**/*.kt")
        trimTrailingWhitespace()
        endWithNewline()
    }
}

detekt {
    buildUponDefaultConfig = true
    config.setFrom(files("detekt-config.yml"))
}

spotlessChangelog {
    setAppendDashSnapshotUnless_dashPrelease(true)
    tagPrefix("v")
    commitMessage("Release v{{version}}")
    remote("origin")
    branch("master")
}

publishing {
    publications.mapNotNull { it as? MavenPublication }.forEach {
        it.groupId = project.group.toString()
        it.artifactId = "daflkt"
        it.version = project.version.toString() +
                if (project.status.toString() == "development") "-SNAPSHOT" else ""

        it.pom {
            name.set("DaFLKt")
            description.set(
                "DaFLKt is a data filtering language for Kotlin to allow " +
                        "a natural language approach to filtering."
            )
            inceptionYear.set("2020")
            url.set("https://gitlab.com/islandoftex/libraries/daflkt")
            organization {
                name.set("Island of TeX")
                url.set("https://gitlab.com/islandoftex")
            }
            licenses {
                license {
                    name.set("BSD 3-clause \"New\" or \"Revised\" License")
                    url.set("https://gitlab.com/islandoftex/libraries/daflkt/blob/master/LICENSE.md")
                }
            }
            scm {
                connection.set("scm:git:git://gitlab.com/islandoftex/libraries/daflkt.git")
                developerConnection.set("scm:git:ssh://git@gitlab.com:islandoftex/libraries/daflkt.git")
                url.set("https://gitlab.com/islandoftex/libraries/daflkt")
            }
            ciManagement {
                system.set("GitLab")
                url.set("https://gitlab.com/islandoftex/libraries/daflkt/pipelines")
            }
            issueManagement {
                system.set("GitLab")
                url.set("https://gitlab.com/islandoftex/libraries/daflkt/issues")
            }
        }
    }

    repositories {
        maven {
            name = "GitLab"
            url = uri("https://gitlab.com/api/v4/projects/23253614/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                if (project.hasProperty("jobToken")) {
                    name = "Job-Token"
                    value = project.property("jobToken").toString()
                }
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
